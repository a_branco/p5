#!/bin/bash

# Get available sessions - Todd Server todd:id=SessionPoll Available Sessions
AVAILABLE_SESSIONS=$(/opt/nagios/libexec/check_jmx -U service:jmx:rmi:///jndi/rmi://ttm1b:10500/jmxrmi -O "todd:id=SessionPool" -A AvailableSessions | awk -F '|' '{print $1}' | awk -F '=' '{print $2}')

# Get size
SIZE=$(/opt/nagios/libexec/check_jmx -U service:jmx:rmi:///jndi/rmi://ttm1b:10500/jmxrmi -O "todd:id=SessionPool" -A Size | awk -F '|' '{print $1}' | awk -F '=' '{print $2}')

# Calculate the percentage of available sessions
PERCENTAGE=$(($AVAILABLE_SESSIONS/$SIZE))

LIMIT=0.2

if [[ "$PERCENTAGE" < "$LIMIT" ]]; then
        echo "TODD AVAILABLE SESSIONS CRITICAL - < 20%"
        exit 2
else
        echo "TODD AVAILABLE SESSIONS OK - > 20%"
        exit 0
fi