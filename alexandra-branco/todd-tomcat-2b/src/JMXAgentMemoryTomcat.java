package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;
import javax.management.openmbean.CompositeDataSupport;
public class JMXAgentMemoryTomcat {

	/**
	 * The first two lines here create and register a GaugeMonitor MBean named
	 * todd:id=SessionPoolMonitor. The next seven lines set attributes that tell
	 * GaugeMonitor which attribute of which MBean should be monitored
	 * (ObservedAttribute or ObservedObject), how often (GranularityPeriod, in
	 * milliseconds), and whether or not to send a notification on high-threshold
	 * and low-threshold violations. Then we invoke the setThresholds() method, via
	 * the MBeanServer, to set the actual high and low threshold values. Finally, we
	 * make the server listen for session pool monitor notifications and start the
	 * gauge monitor.
	 */
	public static void configureMonitor1(MBeanServerConnection mbs) throws Exception {
		ObjectName spmon = new ObjectName("java.lang:type=Memory");

		Set<ObjectInstance> mbeans = mbs.queryMBeans(spmon, null);

		if (mbeans.isEmpty()) {
			mbs.createMBean("javax.management.monitor.GaugeMonitor", spmon);
		}

		javax.management.openmbean.CompositeDataSupport res = (CompositeDataSupport) mbs
				.getAttribute(new ObjectName("java.lang:type=Memory"), "HeapMemoryUsage");
		
		double maxMemory = Long.parseLong(res.get("max").toString());
		double usedMemory = Long.parseLong(res.get("used").toString());
		double percent = Math.round(usedMemory / maxMemory * 100);
		System.out.println("Used memory = " + usedMemory + "; total = " + maxMemory + "; percentage = " + percent + "%");

		long initThreshold = (long) (maxMemory * 0.10);
		
		System.out.println("Memory limit before notification = " + initThreshold);
		
		AttributeList spmal = new AttributeList();
		spmal.add(new Attribute("ObservedObject", new ObjectName("java.lang:type=Memory")));
		spmal.add(new Attribute("ObservedAttribute", "HeapMemoryUsage"));
		spmal.add(new Attribute("GranularityPeriod", new Long(1000)));
		spmal.add(new Attribute("Notify", new Boolean(true)));
		spmal.add(new Attribute("InitThreshold", new Long(initThreshold)));
		mbs.setAttributes(spmon, spmal);

		mbs.addNotificationListener(spmon, new JMXNotificationsMemoryTomcat(), null, null);

		//		mbs.invoke(spmon, "start", new Object[] {}, new String[] {});
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		System.out.println("Todd ClientApp4... Using JMX Notifications and call remote method grow() ");

		try {

			String server = "192.168.2.36:10500";

			if (args.length >= 1) {
				server = args[0];
			}

			System.out.println("Connecting to TODD server at " + server + " ...");

			// Connect to a remote MBean Server
			JMXConnector c = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + server + "/jmxrmi"));

			MBeanServerConnection mbs = c.getMBeanServerConnection();

			System.out.println("Setting up notification handlers...");

			// Set a Notification Handler
			configureMonitor1(mbs);

			System.out.println("Waiting to receive notifications");
			Thread.sleep(500000);

			c.close();
		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
		}
	}
}
