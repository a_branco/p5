package net.jnjmx.todd;

import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;
import javax.management.openmbean.CompositeDataSupport;

public class JMXNotificationsMemoryTomcat implements NotificationListener {
	@Override
	public void handleNotification(Notification notification, Object handback) {
		System.out.println("Received Notification");
		System.out.println("======================================");
		try {
			String tomcatServer = "192.168.2.36";
			JMXConnector tomcatConnector = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + tomcatServer + "/jmxrmi"));

			MBeanServerConnection mbs = tomcatConnector.getMBeanServerConnection();
			ObjectName spmon = new ObjectName("java.lang:type=Memory");
			mbs.invoke(spmon, "gc", null, null);
			System.out.println("Garbage collector called");
			javax.management.openmbean.CompositeDataSupport res = (CompositeDataSupport) mbs
					.getAttribute(new ObjectName("java.lang:type=Memory"), "HeapMemoryUsage");
			
			double maxMemory = Double.parseDouble(res.get("max").toString());
			double usedMemory = Double.parseDouble(res.get("used").toString());
			double percent = Math.round(usedMemory / maxMemory * 100);
			if (percent < 10) {
				System.out.println("The memmory percentage is at a good level at " + percent + "%");
			} else {
				System.out.println("The memory percentage is high at " + percent + "%");
				
				System.out.println("Restarting Tomcat with more memory!");
				String comando = "export _JAVA_OPTIONS=\"-Xmx2G\"";
				comando += "\n";
				comando += "/usr/local/nagios/libexec/check_nrpe -H 192.168.56.11 -c restart_tomcat";
				try {
					ProcessBuilder processBuilder = new ProcessBuilder();
					processBuilder.command("bash", "-c", comando);
					Process process = processBuilder.start();
					process.waitFor();
				} catch (Exception exc) {
				}
			}
		} catch (Exception exc) {
			System.out.println(exc.toString());
		}
		System.out.println("======================================");
	}
}
