package net.jnjmx.todd;

import java.util.Set;

import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;

public class JMXNotificationsGrow implements NotificationListener {
	@Override
	public void handleNotification(Notification notification, Object handback) {
		System.out.println("Received Notification");
		System.out.println("======================================");
		try {
			String server = "192.168.1.2:10500";
			int growNumber = 2;

			System.out.println("Connecting to JMX Agent (with running TODD server) at " + server + " ...");

			// Connect to a remote MBean Server
			JMXConnector c = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + server + "/jmxrmi"));

			MBeanServerConnection mbs = c.getMBeanServerConnection();

			ObjectName son2 = new ObjectName("todd:id=SessionPool");

			// Invoking operation to grow
			mbs.invoke(son2, "grow", new Integer[] { new Integer(growNumber) }, new String[] { "int" });
			System.out.println("Grow of " + growNumber + " sent to server.");
			c.close();

		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
		}
		System.out.println("======================================");
	}
}
