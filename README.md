# P5  

## Parte comum 

### Quagga  
  
```
config t
interface eth0  
ip address 192.168.1.254/24  
!  
interface eth1  
ip address 192.168.2.30/24  
!  
interface eth2  
ip address 192.168.3.3/24  
!  
router ospf  
net 192.168.1.0/24 area 0  
net 192.168.2.0/24 area 0  
net 192.168.3.0/24 area 0  
exit  
exit
w
```

### Nagios  
  

```
#
# This is a sample network config uncomment lines to configure the network
#

auto eth0
iface eth0 inet static
	address 192.168.3.1
	netmask 255.255.255.0
	gateway 192.168.3.3

# DHCP config for eth0
# auto eth0
# iface eth0 inet dhcp
```

## 1200146 Clemente Moreira  

![Architecture.png](./alexandra-branco/imgs/rede_clemente.PNG)

### Web-Term-A
  

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.1.3
	netmask 255.255.255.0
	gateway 192.168.1.254
``` 

### Jenkins-A

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.1.6
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp

```

### Ansible-A  

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.1.5
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp

```
  
  
### Nexus-A

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.1.4
	netmask 255.255.255.0
	gateway 192.168.122.255
```

### Todd-A+Tomcat-A-1

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.1.1
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf
    up /opt/tomcat/bin/startup.sh

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp
```

### Todd-A+Tomcat-A-2

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.1.2
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf
    up /opt/tomcat/bin/startup.sh

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp
```

## 1182161 Alexandra Branco  

![Architecture.png](./alexandra-branco/imgs/Architecture.png)

### Web-Term-B  
  

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.2.32
	netmask 255.255.255.0
	gateway 192.168.2.30
``` 

### Jenkins-B  

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.2.31
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp

```

### Ansible-B  

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.2.34
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp

```
  
  
### Nexus-B

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.2.33
	netmask 255.255.255.0
	gateway 192.168.2.30
```

### Todd-B+Tomcat-B-1

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.2.35
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf
    up /opt/tomcat/bin/startup.sh

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp
```

### Todd-B+Tomcat-B-2

```
#
# This is a sample network config uncomment lines to configure the network
#


# Static config for eth0
auto eth0
iface eth0 inet static
	address 192.168.2.36
	netmask 255.255.255.0
	gateway 192.168.122.255
	up echo nameserver 8.8.8.8 8.8.4.4  > /etc/resolv.conf
    up /opt/tomcat/bin/startup.sh

# DHCP config for eth0
auto eth1
iface eth1 inet dhcp
```

### ssh nos containers
apt-get install ssh
service ssh start


### publish over ssh plugin no jenkins
(chave pública e privada)  
https://nozaki.me/roller/kyle/entry/articles-jenkins-sshdeploy


### Nexus  
Demasiado pesado, máquina limitada de recursos e por isso arranjou-se uma alternativa diferente para enviar os artefactos da máquina do jenkins para a máquina do ansible através do plugin ssh plugin


### configurações do ansible 
/etc/ansible/hosts: adicionar os hosts aqui

geração da chave privada e da chave pública, testar com o m ping  

Playbook para instalar o tood nos hosts

```
---

- name: Network Getting Started First Playbook
  hosts: project_5
  tasks:
    - name: Update packages
      apt: upgrade=dist force_apt_get=yes

    - name: Install java
      apt:
        name: openjdk-8-jdk-headless
        state: latest

    - name: copy jar to host
      ansible.builtin.copy:
        src: /root/deploys/COGSI_P5-1.0.1.jar
        dest: /root/teste
        owner: root
        group: root
        mode: '777'
```


### Jenkins  

Criação do projeto COGSI_P5 que possui um manual step para deploy atráves do ansible 
Criação de uma view no jenkins para o trigger manual do deploy

Para este projeto era requisito a utilização do Jenkins, para criação de uma pipeline que permita sempre que existe uma nova versão da aplicação Todd, que esta seja *deployed* nos 2 *containers* (Todd-B+Tomcat-B-1 e Todd-B+Tomcat-B-2) representados na imagem da secção GNS3.  
Esta pipeline deve guardar os artefactos gerados (.jar da aplicação), enviá-los para um repositório de artefactos (Nexus) e permitir (através da confirmação do utilizador) fazer o *deploy* da aplicação, para este efeito deve ser usado o Ansible.  


Neste projeto o Jenkins encontra-se num *container* Docker, cujo Dockerfile está localizado em: 

Como todo o projeto se encontra “montado” no GNS3, deve sempre ser utilizado o Web-Term-B para acesso às várias interfaces. 

Acedendo então, ao endereço 192.168.2.31:8080 iremos ter o Jenkins. Após a configuração inicial (configuração do utilizador, instalação de *plugins*, ...) foi então criada a pipeline de CI/CD.  

#### COGSI_P5 (Freestyle project)  

(diagrama da pipeline)  

**Source Code Management**  
O primeiro passo desta pipeline é ir buscar a última versão do repositório da aplicação Todd, e para isso é necessário configurar previamente as credenciais e adicionar então o repositório:  
  
![SourceCodeManagement.png](./alexandra-branco/imgs/jenkins-doc/source_code_management.png)


**Build**  

Os passos da pipeline são sequencias, e por isso depois de termos acesso ao conteúdo do repositório precisamos de fazer *build* da aplicação, para que possa ser gerado o ficheiro .jar. 

![Build.png](./alexandra-branco/imgs/jenkins-doc/build.png)


**Post-build Actions**  
É então depois preciso guardar os artefactos gerados e para isso utiliza-se uma *post-build action*:  

![SaveArtifacts.png](./alexandra-branco/imgs/jenkins-doc/post_build_save_artifacts.png)

Para o *deploy* da aplicação deve ser pedida a confirmação do utilizador, para isso é também criada um *post-build action*, mas desta vez um passo manual:  

![DeployManual.png](./alexandra-branco/imgs/jenkins-doc/post_build_deploy_manual.png)


#### P5_Deploy (Freestyle project)  

O passo manual de *deploy* da aplicação fica à espera do *trigger* dado pelo utilizador, quando o utilizador confirma o *deploy* um outro projeto Jenkins é invocado. Este projeto é bastante simples, uma vez que só possui o envio do ficheiro .jar para o *container* do Ansible e posteriormente executa remotamente o comando que irá executar o *playbook* criado.  

Para ser possível o envio de ficheiros via ssh entre máquinas é necessária a instalação de um *plugin* e algumas configurações no Jenkins. 

Plugin Publish Over SSH: https://plugins.jenkins.io/publish-over-ssh 

### Configurações plugin 
### Geração de chave pública e privada no Jenkins para comunicação ssh com o Ansible

No *container* do Jenkins é necessária a geração de um par de chaves pública e privada, para isso executar o seguinte comando:  

```
ssh-keygen -t rsa -b 4096 -m PEM
```

**Nota**: estas chaves devem ficar armazenadas na diretoria: ``/var/jenkins_home/keys``.

Agora é preciso que o Ansible tenha conhecimento desta chave pública do Jenkins, para isso na diretoria ``/root/.ssh/``, criar um ficheiro chamado ``authorized_keys`` e colocar a chave ``id_rsa.pub`` gerada no comando anterior.



Manage Jenkins -> Configure System -> Publish Over ssh:  


![PublishOverSSH.png](./alexandra-branco/imgs/jenkins-doc/PublishOverSSH-1.png)  

  
![PublishOverSSH.png](./alexandra-branco/imgs/jenkins-doc/PublishOverSSH-2.png)



Nos containers instalar o serviço ssh: apt-get update && apt-get install ssh
e iniciar com: service ssh start